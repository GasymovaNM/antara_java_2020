import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Locatable;
import org.openqa.selenium.support.ui.Select;

public class Avito {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("https://www.avito.ru/");

        Select select = new Select(driver.findElement(By.cssSelector("#category")));
        select.getOptions().forEach(webElement -> {
            System.out.println(webElement.getText());
        });
        select.selectByVisibleText("Оргтехника и расходники");

        driver.findElement(By.xpath("//input[@id='search']")).sendKeys("принтер");

        driver.findElement(By.xpath("//div[contains(@data-marker,'search-form/region')]")).click();
        Thread.sleep(1000);

        driver.findElement(By.xpath("//input[@placeholder='Город, регион или Россия']")).sendKeys("Владивосток");

        Thread.sleep(1000);
        driver.findElement(By.xpath("//li[@data-marker='suggest(0)']")).click();

        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[@data-marker='popup-location/save-button']")).click();

        Thread.sleep(1000);
        WebElement checkbox = driver.findElement(By.xpath("//input[@data-marker='delivery-filter/input']"));
        if (checkbox.isSelected()) {
            System.out.println("Checkbox is Toggled On");
        } else {
            System.out.println("Checkbox is Toggled Off");
            int x = checkbox.getLocation().getX();
            int y = checkbox.getLocation().getY();
            ((JavascriptExecutor) driver).executeScript("scroll(" + x + "," + y + ")");
            Actions actions = new Actions(driver);
            actions.moveToElement(checkbox).click().perform();
        }

        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[@data-marker='search-filters/submit-button']")).click();

        WebElement option = driver.findElement(By.xpath("//option[normalize-space(text()) = 'Дороже']"));
        option.click();

        Thread.sleep(1000);
        List<WebElement> elements = driver.findElements(By.xpath("//div[@data-marker='catalog-serp']/descendant::div[@data-marker='item'][position() <= 3]"));
        for (WebElement element : elements) {
            String name = element.findElement(By.cssSelector("span[itemprop='name']")).getText();
            String price = element.findElement(By.cssSelector("span[data-marker='item-price']")).getText();
            System.out.printf("Name = '%s'; price = %s\n", name, price);
        }


    }
}

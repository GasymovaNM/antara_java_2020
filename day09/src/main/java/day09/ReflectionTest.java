package day09;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class ReflectionTest {
    public static void main(String[] args) throws Exception {
        Class manClass = new Man(1, "Bob", 25).getClass();
        Method[] methods = manClass.getMethods();

        System.out.println("\n--------------manClass.getMethods()---------------");

        for (Method method : methods) {
            printMethodInfo(method);
        }
        System.out.println("\n--------------manClass.getSuperclass().getMethods()---------");

        for (Method method : manClass.getSuperclass().getMethods()) {
            printMethodInfo(method);
        }

        System.out.println("\n-------------manClass.getDeclaredMethods()----------------");
        Method[] methods1 = manClass.getDeclaredMethods();
        for (Method method : methods1) {
            printMethodInfo(method);
        }
        System.out.println("\n-------------manClass.getSuperclass().getDeclaredMethods()----------------");
        Method[] methods2 = manClass.getSuperclass().getDeclaredMethods();
        for (Method method : methods2) {
            printMethodInfo(method);
        }
        System.out.println("\n-------------manClass.getFields()----------------");
        Field[] fields = manClass.getFields();
        for (Field field : fields) {
            printFieldInfo(field);
        }
        System.out.println("\n-------------manClass.getDeclaredFields()----------------");
        Field[] fields1 = manClass.getDeclaredFields();
        for (Field field : fields1) {
            printFieldInfo(field);
        }
        System.out.println("\n-------------manClass.getSuperclass().getDeclaredFields()----------------");
        Field[] fields2 = manClass.getSuperclass().getDeclaredFields();
        for (Field field : fields2) {
            printFieldInfo(field);
        }
        System.out.println("\n-------------manClass.getConstructors()----------------");
        Constructor[] constructors = manClass.getConstructors();
        for (Constructor constructor : constructors) {
            printConstructorInfo(constructor);
        }
        System.out.println("\n-------------manClass.getDeclaredConstructors()----------------");
        Constructor[] constructors1 = manClass.getDeclaredConstructors();
        for (Constructor constructor : constructors1) {
            printConstructorInfo(constructor);
        }
        System.out.println("\n-------------manClass.getSuperclass().getDeclaredConstructors()----------------");
        Constructor[] constructors2 = manClass.getSuperclass().getDeclaredConstructors();
        for (Constructor constructor : constructors2) {
            printConstructorInfo(constructor);
        }

        System.out.println("\n-------------Create Man by class and invoke private method----------------");
        Constructor<Man> constructor = Man.class.getConstructor(int.class, String.class, int.class);
        Man tom = constructor.newInstance( 1, "Tom", 40);
        Method toBuildHouse = tom.getClass().getDeclaredMethod("toBuildHouse", int.class);
        toBuildHouse.setAccessible(true);
        toBuildHouse.invoke(tom, 10);

        System.out.println("\n-------------Change private field----------------");
        Field age = tom.getClass().getDeclaredField("age");
        age.setAccessible(true);
        age.set(tom, 15);
        System.out.println("Tom is " + tom.getAge() + " years old");
    }

    private static void printFieldInfo(Field field) {
        System.out.println(Modifier.toString(field.getModifiers())
                + " " + field.getType().getSimpleName()
                + " " + field.getName());
    }

    private static void printMethodInfo(Method method) {
        System.out.println(Modifier.toString(method.getModifiers())
                + " " + method.getReturnType().getSimpleName()
                + " " + method.getName()
                + " " + Arrays.toString(method.getParameterTypes()));
    }

    private static void printConstructorInfo(Constructor constructor) {
        System.out.println(Modifier.toString(constructor.getModifiers())
                + " " + constructor.getName()
                + " " + Arrays.toString(constructor.getParameterTypes()));
    }

}
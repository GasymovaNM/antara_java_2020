package day09;

public abstract class Person {
    private String name;
    public int id;

    public Person(int id, String name) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    private void sayHello() {
        System.out.println("Person with id: " + id + " and name: " + name + " says hello");
    }

    private void sayGoodbye() {
        System.out.println("Person with id: " + id + " and name: " + name + " says goodbye");
    }
}

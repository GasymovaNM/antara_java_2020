package day09;

public class Man extends Person implements Swimmable {
    public String address;
    private int age;

    public Man(int id, String name, int age) {
        super(id, name);
        if (age >= 0) {
            this.age = age;
        } else {
            System.out.println("Ошибка! Возраст не может быть отрицательным числом!");
        }
    }

    protected Man(int id, String name, String address) {
        super(id, name);
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    private void toPlantTree() {
        System.out.println(getName() + " сажает дерево");

    }

    private void toBuildHouse(int years) {
        System.out.println(getName() + " строит дом " + years + " лет");

    }

    private void raiseTheSon() {
        System.out.println(getName() + " воспитывает сына");
    }

    public void swim() {
        System.out.println(getName() + " умеет плавать");
    }

}

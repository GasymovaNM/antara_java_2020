package day04.aviary;

import java.util.HashMap;
import java.util.Map;
import day04.Feedable;
import day04.FoodMaker;
import day04.animals.Animal;
import day04.food.Food;

public class Aviary<T extends Animal> {

    private int capacity;
    private String name;
    private Map<String, T> animals;

    public Aviary(AviarySize aviarySize, String name) {
        this.animals = new HashMap<String, T>();
        this.capacity = aviarySize.getValue();
        this.name = name;
    }

    public void sayAnimalNames() {
        for (Map.Entry<String, T> entry : animals.entrySet()) {
            T animal = entry.getValue();
            animal.sayName();
        }
    }

    public void addAnimal(T a) {
        if (a.getSize().getValue() <= capacity) {
            animals.put(a.getName(), a);
            capacity = capacity - a.getSize().getValue();
            System.out.println(a.getClass().getSimpleName() + " " + a.getName() + " was added to '" + name + "' (remaining capacity = " + capacity + ")");
        } else {
            System.out.println("There is no enough space for " + a.getClass().getSimpleName() + " " + a.getName() + " in '" + name + "' (remaining capacity = " + capacity + ")");
        }
    }

    public void deleteAnimal(String name) {
        T removed = animals.remove(name);
        if (removed != null) {
            capacity = capacity + removed.getSize().getValue();
            System.out.println(removed.getClass().getSimpleName() + " " + removed.getName() + " was deleted from '" + this.name + "' (remaining capacity = " + capacity + ")");
        }
    }

    public T findByName(String s) {
        return animals.get(s);
    }

    /**
     * This method is automatic food feeder.
     */
    public void feedAll() {
        FoodMaker foodMaker = new FoodMaker();

        for (Map.Entry<String, T> entry : animals.entrySet()) {
            T value = entry.getValue();
            if (value instanceof Feedable) {
                Feedable f = (Feedable) value;
                Food.Products whatToEat = f.sayWhatToEat();
                f.eat(foodMaker.createFood(whatToEat));
            }
        }
    }
}

package day04;

import day04.animals.Animal;
import day04.animals.birds.Duck;
import day04.animals.carnivorous.Crocodile;
import day04.animals.herbivore.Elephant;
import day04.animals.herbivore.Koala;
import day04.animals.carnivorous.Lion;
import day04.animals.herbivore.Rabbit;
import day04.animals.carnivorous.Shark;

public class AnimalMaker {

    public Animal createAnimal(Animal.Animals type, String name) {
        Animal animal;

        switch (type) {
            case LION:
                animal = new Lion(name);
                break;
            case SHARK:
                animal = new Shark(name);
                break;
            case RABBIT:
                animal = new Rabbit(name);
                break;
            case KOALA:
                animal = new Koala(name);
                break;
            case ELEPHANT:
                animal = new Elephant(name);
                break;
            case CROCODILE:
                animal = new Crocodile(name);
                break;
            case DUCK:
                animal = new Duck(name);
                break;
            default:
                throw new RuntimeException("There's no such type of animal in the zoo: " + type);
        }
        return animal;
    }
}

package day04.animals;

import day04.food.WrongFoodException;
import day04.food.Food;

public abstract class Animal {

    private String name;
    private AnimalSize size;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    protected void setSize (AnimalSize size) {
        this.size = size;
    }

    public AnimalSize getSize() {
        return size;
    }

    public enum Animals {
        LION,
        CROCODILE,
        SHARK,
        ELEPHANT,
        RABBIT,
        KOALA,
        DUCK
    }

    public void sayName() {
        System.out.println("Hi! I am a " + getClass().getSimpleName() + ", my name is " + getName());
    }

    public void eat(Food food) {
        try {
            checkFood(food);
            System.out.println(getClass().getSimpleName() + " " + name + " is eating " + food.getClass().getSimpleName());
        } catch (WrongFoodException e) {
            System.out.println("UGH! I can't eat this food: " + food.getClass().getSimpleName());
        }
    }

    protected abstract void checkFood(Food food) throws WrongFoodException;
}

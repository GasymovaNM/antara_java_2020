package day04.animals.birds;

import day04.animals.Animal;
import day04.food.Food;

public abstract class Bird extends Animal {

    public Bird(String name) {
        super(name);
    }

    @Override
    protected void checkFood(Food food) {
        throw new UnsupportedOperationException();
    }



}

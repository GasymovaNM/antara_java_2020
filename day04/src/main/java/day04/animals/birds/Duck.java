package day04.animals.birds;

import day04.Feedable;
import day04.animals.AnimalSize;
import day04.food.Food;

/**
 * The duck is omnivorous
 */
public class Duck extends Bird implements Feedable {

    public Duck(String name) {
        super(name);
        setSize(AnimalSize.SMALL);
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.getRandomProduct();
    }

    public void eat(Food food) {
        System.out.println(getClass().getSimpleName() + " " + getName() + " is eating " + food.getClass().getSimpleName());
    }
}

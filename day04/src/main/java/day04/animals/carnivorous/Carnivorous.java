package day04.animals.carnivorous;

import day04.food.WrongFoodException;
import day04.animals.Mammal;
import day04.food.Food;
import day04.food.meat.MeatFood;

public abstract class Carnivorous extends Mammal {

    public Carnivorous(String name) {
        super(name);
    }

    @Override
    protected void checkFood(Food food) throws WrongFoodException {
        boolean isSuitableFood = food instanceof MeatFood;
        if (!isSuitableFood) {
            throw new WrongFoodException();
        }
    }
}

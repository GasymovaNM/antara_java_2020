package day04.animals.carnivorous;

import day04.Feedable;
import day04.animals.AnimalSize;
import day04.food.Food;

public class Lion extends Carnivorous  implements Feedable {

    public Lion(String name) {
        super(name);
        setSize(AnimalSize.MIDDLE);
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.BEEF;
    }
}

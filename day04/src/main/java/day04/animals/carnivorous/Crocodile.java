package day04.animals.carnivorous;

import day04.Feedable;
import day04.animals.AnimalSize;
import day04.food.Food;

public class Crocodile extends Carnivorous implements Feedable {

    public Crocodile(String name) {
        super(name);
        setSize(AnimalSize.BIG);
    }


    public Food.Products sayWhatToEat() {
        return Food.Products.CARRION;
    }
}

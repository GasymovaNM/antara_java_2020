package day04.animals.carnivorous;

import day04.Feedable;
import day04.animals.AnimalSize;
import day04.food.Food;

public class Shark extends Carnivorous implements Feedable {

    public Shark(String name) {
        super(name);
        setSize(AnimalSize.BIG);

    }

    public Food.Products sayWhatToEat() {
        return Food.Products.PLANKTON;
    }
}

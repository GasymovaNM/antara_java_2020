package day04.animals.herbivore;

import day04.food.WrongFoodException;
import day04.animals.Mammal;
import day04.food.Food;
import day04.food.plants.PlantFood;

public abstract class Herbivore extends Mammal {

    public Herbivore(String name) {
        super(name);
    }

    @Override
    protected void checkFood(Food food) throws WrongFoodException {
        boolean isSuitableFood = food instanceof PlantFood;
        if (!isSuitableFood) {
            throw new WrongFoodException();
        }
    }

}

package day04.animals.herbivore;

import day04.Feedable;
import day04.animals.AnimalSize;
import day04.food.Food;

public class Rabbit extends Herbivore implements Feedable {

    public Rabbit(String name) {
        super(name);
        setSize(AnimalSize.SMALL);
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.CARROT;
    }
}

package day04.animals.herbivore;

import day04.Feedable;
import day04.animals.AnimalSize;
import day04.food.Food;

public class Koala extends Herbivore implements Feedable {

    public Koala(String name) {
        super(name);
        setSize(AnimalSize.SMALL);
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.EUCALYPTUS;
    }
}

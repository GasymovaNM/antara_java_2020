package day04.animals.herbivore;

import day04.Feedable;
import day04.animals.AnimalSize;
import day04.food.Food;

public class Elephant extends Herbivore implements Feedable {

    public Elephant(String name) {
        super(name);
        setSize(AnimalSize.LARGE);
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.BANANA;
    }
}

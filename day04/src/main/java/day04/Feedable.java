package day04;

import day04.food.Food;

public interface Feedable {
    void eat(Food food);

    Food.Products sayWhatToEat();

}

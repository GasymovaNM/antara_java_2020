package day04.food;

import java.util.Random;

public abstract class Food {
    public enum Products {
        BEEF,
        PLANKTON,
        CARRION,
        BANANA,
        CARROT,
        EUCALYPTUS;

        public static Products getRandomProduct() {
            Random random = new Random();
            Products[] values = values();
            return values[random.nextInt(values.length)];
        }
    }
}

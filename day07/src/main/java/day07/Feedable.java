package day07;

import day07.food.Food;

public interface Feedable {
    void eat(Food food);

    Food.Products sayWhatToEat();

}

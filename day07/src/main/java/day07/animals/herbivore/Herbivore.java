package day07.animals.herbivore;

import day07.food.WrongFoodException;
import day07.animals.Mammal;
import day07.food.Food;
import day07.food.plants.PlantFood;

public abstract class Herbivore extends Mammal {

    public Herbivore(String name) {
        super(name);
    }

    public Herbivore() {
    }

    @Override
    protected void checkFood(Food food) throws WrongFoodException {
        boolean isSuitableFood = food instanceof PlantFood;
        if (!isSuitableFood) {
            throw new WrongFoodException();
        }
    }

}

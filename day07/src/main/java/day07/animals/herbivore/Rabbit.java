package day07.animals.herbivore;

import day07.Feedable;
import day07.animals.AnimalSize;
import day07.food.Food;

public class Rabbit extends Herbivore implements Feedable {

    public Rabbit(String name) {
        super(name);
        setSize(AnimalSize.SMALL);
    }

    public Rabbit() {
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.CARROT;
    }
}

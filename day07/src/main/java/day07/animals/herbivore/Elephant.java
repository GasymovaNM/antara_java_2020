package day07.animals.herbivore;

import day07.Feedable;
import day07.animals.AnimalSize;
import day07.food.Food;

public class Elephant extends Herbivore implements Feedable {

    public Elephant(String name) {
        super(name);
        setSize(AnimalSize.LARGE);
    }

    public Elephant() {
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.BANANA;
    }
}

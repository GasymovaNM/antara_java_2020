package day07.animals.herbivore;

import day07.Feedable;
import day07.animals.AnimalSize;
import day07.food.Food;

public class Koala extends Herbivore implements Feedable {

    public Koala(String name) {
        super(name);
        setSize(AnimalSize.SMALL);
    }

    public Koala() {
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.EUCALYPTUS;
    }
}

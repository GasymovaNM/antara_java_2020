package day07.animals.carnivorous;

import day07.Feedable;
import day07.animals.AnimalSize;
import day07.food.Food;

public class Lion extends Carnivorous  implements Feedable {

    public Lion(String name) {
        super(name);
        setSize(AnimalSize.MIDDLE);
    }

    public Lion() {
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.BEEF;
    }
}

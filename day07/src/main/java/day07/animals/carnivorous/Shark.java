package day07.animals.carnivorous;

import day07.Feedable;
import day07.animals.AnimalSize;
import day07.food.Food;

public class Shark extends Carnivorous implements Feedable {

    public Shark(String name) {
        super(name);
        setSize(AnimalSize.BIG);
    }

    public Shark() {
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.PLANKTON;
    }
}

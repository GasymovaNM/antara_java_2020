package day07.animals.carnivorous;

import day07.Feedable;
import day07.animals.AnimalSize;
import day07.food.Food;

public class Crocodile extends Carnivorous implements Feedable {

    public Crocodile(String name) {
        super(name);
        setSize(AnimalSize.BIG);
    }

    public Crocodile() {
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.CARRION;
    }
}

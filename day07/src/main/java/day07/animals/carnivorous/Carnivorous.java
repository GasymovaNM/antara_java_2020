package day07.animals.carnivorous;

import day07.food.WrongFoodException;
import day07.animals.Mammal;
import day07.food.Food;
import day07.food.meat.MeatFood;

public abstract class Carnivorous extends Mammal {


    public Carnivorous(String name) {
        super(name);
    }

    public Carnivorous() {
    }

    @Override
    protected void checkFood(Food food) throws WrongFoodException {
        boolean isSuitableFood = food instanceof MeatFood;
        if (!isSuitableFood) {
            throw new WrongFoodException();
        }
    }
}

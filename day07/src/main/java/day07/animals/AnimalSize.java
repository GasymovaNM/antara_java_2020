package day07.animals;

public enum AnimalSize {
    SMALL(1),
    MIDDLE(5),
    BIG(7),
    LARGE(10);

    private int value;

    AnimalSize(int i) {
        this.value = i;
    }

    public int getValue() {
        return value;
    }
}

package day07.animals;

public abstract class Mammal extends Animal {

    public Mammal(String name) {
        super(name);
    }

    public Mammal() {
    }
}

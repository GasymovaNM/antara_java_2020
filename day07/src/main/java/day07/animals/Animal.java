package day07.animals;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import day07.food.WrongFoodException;
import day07.food.Food;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public abstract class Animal implements Serializable {
    private static Logger logger = LoggerFactory.getLogger(Animal.class);

    private String name;

    private AnimalSize size;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    protected void setSize(AnimalSize size) {
        this.size = size;
    }

    public AnimalSize getSize() {
        return size;
    }

    public Animal() {
    }

    public enum Animals {
        LION,
        CROCODILE,
        SHARK,
        ELEPHANT,
        RABBIT,
        KOALA,
        DUCK
    }

    public void sayName() {
        logger.info("Hi! I am a " + getClass().getSimpleName() + ", my name is " + getName());
    }

    public void eat(Food food) {
        try {
            checkFood(food);
            logger.info(getClass().getSimpleName() + " " + name + " is eating " + food.getClass().getSimpleName());
        } catch (WrongFoodException e) {
            logger.error("UGH! I can't eat this food: " + food.getClass().getSimpleName());
        }
    }

    protected abstract void checkFood(Food food) throws WrongFoodException;
}

package day07.animals.birds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import day07.Feedable;
import day07.animals.AnimalSize;
import day07.food.Food;

/**
 * The duck is omnivorous
 */
public class Duck extends Bird implements Feedable {
    private static Logger logger = LoggerFactory.getLogger(Duck.class);

    public Duck(String name) {
        super(name);
        setSize(AnimalSize.SMALL);
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.getRandomProduct();
    }

    public void eat(Food food) {
        logger.info (getClass().getSimpleName() + " " + getName() + " is eating " + food.getClass().getSimpleName());
    }
}

package day07.animals.birds;
import day07.animals.Animal;
import day07.food.Food;

public abstract class Bird extends Animal {

    public Bird(String name) {
        super(name);
    }

    @Override
    protected void checkFood(Food food) {
        throw new UnsupportedOperationException();
    }



}

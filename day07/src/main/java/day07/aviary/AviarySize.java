package day07.aviary;

public enum AviarySize {
    SMALL(5),
    MIDDLE(10),
    BIG(20),
    LARGE(40);

    private final int value;

    AviarySize(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}

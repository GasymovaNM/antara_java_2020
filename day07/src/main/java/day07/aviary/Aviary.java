package day07.aviary;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import day07.Feedable;
import day07.FoodMaker;
import day07.animals.Animal;
import day07.food.Food;

@XmlAccessorType(XmlAccessType.FIELD)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)

public class Aviary<T extends Animal> implements Serializable {
    private static Logger logger = LoggerFactory.getLogger(Animal.class);


    private int capacity;
    private String name;

    @JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
    private Map<String, T> animals;

    public Aviary() {

    }

    public Aviary(AviarySize aviarySize, String name) {
        this.animals = new HashMap<>();
        this.capacity = aviarySize.getValue();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void sayAnimalNames() {
        for (Map.Entry<String, T> entry : animals.entrySet()) {
            Animal animal = entry.getValue();
            animal.sayName();
        }
    }

    public void addAnimal(T a) {
        if (a.getSize().getValue() <= capacity) {
            animals.put(a.getName(), a);
            capacity = capacity - a.getSize().getValue();
            logger.info(a.getClass().getSimpleName() + " " + a.getName() + " was added to '" + name + "' (remaining capacity = " + capacity + ")");
        } else {
            logger.error("There is no enough space for " + a.getClass().getSimpleName() + " " + a.getName() + " in '" + name + "' (remaining capacity = " + capacity + ")");
        }
    }

    public void deleteAnimal(String name) {
        Animal removed = animals.remove(name);
        if (removed != null) {
            capacity = capacity + removed.getSize().getValue();
            logger.info(removed.getClass().getSimpleName() + " " + removed.getName() + " was deleted from '" + this.name + "' (remaining capacity = " + capacity + ")");
        }
    }

    public Animal findByName(String s) {
        return animals.get(s);
    }

    /**
     * This method is automatic food feeder.
     */
    public void feedAll() {
        FoodMaker foodMaker = new FoodMaker();

        for (Map.Entry<String, T> entry : animals.entrySet()) {
            Animal value = entry.getValue();
            if (value instanceof Feedable) {
                Feedable f = (Feedable) value;
                Food.Products whatToEat = f.sayWhatToEat();
                f.eat(foodMaker.createFood(whatToEat));
            }
        }
    }

    @Override
    public String toString() {
        return "Aviary{" +
                "capacity=" + capacity +
                ", name='" + name + '\'' +
                ", animals=" + animals +
                '}';
    }
}

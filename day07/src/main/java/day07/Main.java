package day07;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import day07.animals.Animal;
import day07.animals.carnivorous.Carnivorous;
import day07.animals.herbivore.Herbivore;
import day07.aviary.Aviary;
import day07.aviary.AviarySize;

public class Main {

    private static final String CAR_AVIARY_1 = "aviary for herbivore #1";
    private static final String CAR_AVIARY_2 = "aviary for herbivore #2";
    private static final String CAR_AVIARY_3 = "aviary for herbivore #3";
    private static final String HERB_AVIARY_1 = "aviary for herbivore #1";
    private static final String HERB_AVIARY_2 = "aviary for herbivore #2";

    public static void main(String[] args) throws IOException {
        Zoo zoo;
        File file = new File("day07/src/main/resources/zoo-input.xml");
        XmlMapper xmlMapper = new XmlMapper();
        //Отступы в выходном xml
        xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
        //чтобы работала @XmlAccessorType(XmlAccessType.FIELD) - доступ к приватным полям
        xmlMapper.registerModule(new JaxbAnnotationModule());

        if (file.exists()) {
            zoo = xmlMapper.readValue(file, Zoo.class);
            AnimalMaker animalMaker = new AnimalMaker();
            Aviary<Herbivore> herbivoreAviary = new Aviary<>(AviarySize.LARGE, CAR_AVIARY_3);
            Animal koala1 = animalMaker.createAnimal(Animal.Animals.KOALA, "Koala1");
            herbivoreAviary.addAnimal((Herbivore) koala1);
            List<Aviary<Herbivore>> aviaries = zoo.getHerbAviaries();
            aviaries.add(herbivoreAviary);

            Aviary<Herbivore> herbAviaryByName = zoo.findHerbAviaryByName(HERB_AVIARY_2);
            herbAviaryByName.deleteAnimal("Dumbo2");

            Aviary<Carnivorous> carAviaryByName = zoo.findCarAviaryByName(CAR_AVIARY_2);
            carAviaryByName.deleteAnimal("Scar");

            Aviary<Herbivore> herbAviaryByName1 = zoo.findHerbAviaryByName(HERB_AVIARY_1);
            Animal rabbit1 = animalMaker.createAnimal(Animal.Animals.RABBIT, "Rabbit1");
            herbAviaryByName1.addAnimal((Herbivore) rabbit1);

            Animal lion1 = animalMaker.createAnimal(Animal.Animals.LION, "Lion1");
            Aviary<Carnivorous> carAviaryByName1 = zoo.findCarAviaryByName(CAR_AVIARY_1);
            carAviaryByName1.addAnimal((Carnivorous) lion1);

            File fileOutput = new File("day07/src/main/resources/zoo-output.xml");
            xmlMapper.writeValue(fileOutput, zoo);
        } else {
        zoo = createNewZoo();
        xmlMapper.writeValue(file, zoo);
        }
    }

    private static Zoo createNewZoo() {
        AnimalMaker animalMaker = new AnimalMaker();

        Aviary<Carnivorous> aviaryForCarnivores1 = new Aviary<Carnivorous>(AviarySize.LARGE, CAR_AVIARY_1);
        Animal crocodile1 = animalMaker.createAnimal(Animal.Animals.CROCODILE, "Dundee1");
        Animal crocodile2 = animalMaker.createAnimal(Animal.Animals.CROCODILE, "Dundee2");
        Animal shark = animalMaker.createAnimal(Animal.Animals.SHARK, "Judy");
        aviaryForCarnivores1.addAnimal((Carnivorous) crocodile1);
        aviaryForCarnivores1.addAnimal((Carnivorous) crocodile2);
        aviaryForCarnivores1.addAnimal((Carnivorous) shark);

        Aviary<Carnivorous> aviaryForCarnivores2 = new Aviary<Carnivorous>(AviarySize.BIG, CAR_AVIARY_2);
        Animal lion1 = animalMaker.createAnimal(Animal.Animals.LION, "Mufasa");
        Animal lion2 = animalMaker.createAnimal(Animal.Animals.LION, "Simba");
        Animal lion3 = animalMaker.createAnimal(Animal.Animals.LION, "Scar");
        Animal lion4 = animalMaker.createAnimal(Animal.Animals.LION, "Leo");
        aviaryForCarnivores2.addAnimal((Carnivorous) lion1);
        aviaryForCarnivores2.addAnimal((Carnivorous) lion2);
        aviaryForCarnivores2.addAnimal((Carnivorous) lion3);
        aviaryForCarnivores2.addAnimal((Carnivorous) lion4);

        Aviary<Herbivore> aviaryForHerbivores1 = new Aviary<Herbivore>(AviarySize.SMALL, HERB_AVIARY_1);
        Animal koala = animalMaker.createAnimal(Animal.Animals.KOALA, "Martin");
        Animal rabbit1 = animalMaker.createAnimal(Animal.Animals.RABBIT, "Rodger");
        Animal rabbit2 = animalMaker.createAnimal(Animal.Animals.RABBIT, "Rodger2");
        aviaryForHerbivores1.addAnimal((Herbivore) koala);
        aviaryForHerbivores1.addAnimal((Herbivore) rabbit1);
        aviaryForHerbivores1.addAnimal((Herbivore) rabbit2);

        Aviary<Herbivore> aviaryForHerbivores2 = new Aviary<Herbivore>(AviarySize.LARGE, HERB_AVIARY_2);
        Animal elephant1 = animalMaker.createAnimal(Animal.Animals.ELEPHANT, "Dumbo1");
        Animal elephant2 = animalMaker.createAnimal(Animal.Animals.ELEPHANT, "Dumbo2");
        Animal elephant3 = animalMaker.createAnimal(Animal.Animals.ELEPHANT, "Dumbo3");
        aviaryForHerbivores2.addAnimal((Herbivore) elephant1);
        aviaryForHerbivores2.addAnimal((Herbivore) elephant2);
        aviaryForHerbivores2.addAnimal((Herbivore) elephant3);

        Zoo zoo = new Zoo();
        zoo.getHerbAviaries().addAll(Arrays.asList(aviaryForHerbivores1, aviaryForHerbivores2));
        zoo.getCarAviaries().addAll(Arrays.asList(aviaryForCarnivores1, aviaryForCarnivores2));

        return zoo;
    }


}

package day07;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import day07.animals.Animal;
import day07.animals.carnivorous.Carnivorous;
import day07.animals.herbivore.Herbivore;
import day07.aviary.Aviary;

@JacksonXmlRootElement(localName = "zoo")
public class Zoo implements Serializable {

    @JacksonXmlElementWrapper(localName = "carAviaries")
    @JacksonXmlProperty(localName = "carAviary")
    private List<Aviary<Carnivorous>> carAviaries = new ArrayList<>();

    @JacksonXmlElementWrapper(localName = "herbAviaries")
    @JacksonXmlProperty(localName = "herbAviary")
    private List<Aviary<Herbivore>> herbAviaries = new ArrayList<>();

    public List<Aviary<Carnivorous>> getCarAviaries() {
        return carAviaries;
    }

    public List<Aviary<Herbivore>> getHerbAviaries() {
        return herbAviaries;
    }

    public Aviary<Carnivorous> findCarAviaryByName(String name) {
        return getAviaryByName(name, carAviaries);
    }

    public Aviary<Herbivore> findHerbAviaryByName(String name) {
        return getAviaryByName(name, herbAviaries);
    }

    private <T extends Animal> Aviary<T> getAviaryByName(String name, List<Aviary<T>> aviaries) {
        for (Aviary<T> aviary : aviaries) {
            if (aviary.getName().equals(name)) {
                return aviary;
            }
        }
        return null;
    }
}

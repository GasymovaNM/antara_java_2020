package day03;

import day03.animals.Animal;
import day03.animals.birds.Bird;
import day03.animals.carnivorous.Carnivorous;
import day03.animals.herbivore.Herbivore;
import day03.aviary.Aviary;

public class Main {
    public static void main(String[] args) {
        AnimalMaker animalMaker = new AnimalMaker();

        //--------------------------------- carnivorous --------------------------------
        Animal lion1 = animalMaker.createAnimal(Animal.Animals.LION, "Mufasa");
        Animal lion2 = animalMaker.createAnimal(Animal.Animals.LION, "Simba");
        Animal lion3 = animalMaker.createAnimal(Animal.Animals.LION, "Scar");
        Animal lion4 = animalMaker.createAnimal(Animal.Animals.LION, "Leo");
        Animal lion5 = animalMaker.createAnimal(Animal.Animals.LION, "Nala");
        Animal crocodile1 = animalMaker.createAnimal(Animal.Animals.CROCODILE, "Dundee");
        Animal crocodile2 = animalMaker.createAnimal(Animal.Animals.CROCODILE, "Dundee2");
        Animal shark = animalMaker.createAnimal(Animal.Animals.SHARK, "Judy");

        Aviary<Carnivorous> aviaryForCarnivores1 = new Aviary<Carnivorous>(4);
        Aviary<Carnivorous> aviaryForCarnivores2 = new Aviary<Carnivorous>(3);

        aviaryForCarnivores1.addAnimal((Carnivorous) lion1);
        aviaryForCarnivores1.addAnimal((Carnivorous) lion2);
        aviaryForCarnivores1.addAnimal((Carnivorous) lion3);
        aviaryForCarnivores1.addAnimal((Carnivorous) lion4);
        aviaryForCarnivores1.addAnimal((Carnivorous) lion5);

        aviaryForCarnivores1.deleteAnimal(lion1.getName());

        aviaryForCarnivores1.sayAnimalNames();

        System.out.println(aviaryForCarnivores1.findByName("Simba").getName());

        aviaryForCarnivores2.addAnimal((Carnivorous) crocodile1);
        aviaryForCarnivores2.addAnimal((Carnivorous) crocodile2);
        aviaryForCarnivores2.addAnimal((Carnivorous) shark);
        aviaryForCarnivores2.sayAnimalNames();

        //--------------------------------- herbivore --------------------------------
        Animal koala = animalMaker.createAnimal(Animal.Animals.KOALA, "Martin");
        Animal rabbit = animalMaker.createAnimal(Animal.Animals.RABBIT, "Rodger");
        Animal elephant1 = animalMaker.createAnimal(Animal.Animals.ELEPHANT, "Dumbo");
        Animal elephant2 = animalMaker.createAnimal(Animal.Animals.ELEPHANT, "Dumbo2");

        Aviary<Herbivore> aviaryForHerbivore1 = new Aviary<Herbivore>(2);
        Aviary<Herbivore> aviaryForHerbivore2 = new Aviary<Herbivore>(2);
        Aviary<Bird> birdAviary = new Aviary<Bird>(5);

        aviaryForHerbivore1.addAnimal((Herbivore) koala);
        aviaryForHerbivore1.addAnimal((Herbivore) rabbit);
        aviaryForHerbivore1.sayAnimalNames();

        aviaryForHerbivore2.addAnimal((Herbivore) elephant1);
        aviaryForHerbivore2.addAnimal((Herbivore) elephant2);
        aviaryForHerbivore2.sayAnimalNames();

        aviaryForCarnivores1.feedAll();
        aviaryForCarnivores2.feedAll();
        aviaryForHerbivore1.feedAll();
        aviaryForHerbivore2.feedAll();

        //--------------------------------- Birds --------------------------------
        Animal duck = animalMaker.createAnimal(Animal.Animals.DUCK, "Donalt");
        Animal duck1 = animalMaker.createAnimal(Animal.Animals.DUCK, "Donalt1");
        Animal duck2 = animalMaker.createAnimal(Animal.Animals.DUCK, "Donalt2");
        Animal duck3 = animalMaker.createAnimal(Animal.Animals.DUCK, "Donalt3");

        birdAviary.addAnimal((Bird) duck);
        birdAviary.addAnimal((Bird) duck1);
        birdAviary.addAnimal((Bird) duck2);
        birdAviary.addAnimal((Bird) duck3);

        birdAviary.feedAll();
    }
}

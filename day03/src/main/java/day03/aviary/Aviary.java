package day03.aviary;

import java.util.HashMap;
import java.util.Map;
import day03.Feedable;
import day03.FoodMaker;
import day03.animals.Animal;
import day03.animals.carnivorous.Carnivorous;
import day03.animals.herbivore.Herbivore;
import day03.food.Food;

public class Aviary<T extends Animal> {

    private int size;

    private Map<String, T> animals;

    public Aviary(int capacity) {
        this.animals = new HashMap<String, T>();
        this.size = capacity;
    }

    public void sayAnimalNames() {
        for (Map.Entry<String, T> entry : animals.entrySet()) {
            T animal = entry.getValue();
            animal.sayName();
        }
    }

    public void addAnimal(T a) {
        if (animals.size() < size) {
            animals.put(a.getName(), a);
        } else {
            System.out.println("Aviary is full, there is no space for " + a.getName());
        }
    }

    public void deleteAnimal(String name) {
        animals.remove(name);
    }

    public T findByName(String s) {
        return animals.get(s);
    }

    /**
     * This method is automatic food feeder.
     */
    public void feedAll() {
        FoodMaker foodMaker = new FoodMaker();

        for (Map.Entry<String, T> entry : animals.entrySet()) {
            T value = entry.getValue();
            if (value instanceof Feedable) {
                Feedable f = (Feedable) value;
                Food.Products whatToEat = f.sayWhatToEat();
                f.eat(foodMaker.createFood(whatToEat));
            }
        }
    }
}

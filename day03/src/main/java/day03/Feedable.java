package day03;

import day03.food.Food;

public interface Feedable {
    void eat(Food food);

    Food.Products sayWhatToEat();

}

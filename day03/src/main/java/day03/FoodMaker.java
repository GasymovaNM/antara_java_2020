package day03;

import day03.food.Food;
import day03.food.meat.Beef;
import day03.food.meat.Carrion;
import day03.food.meat.Plankton;
import day03.food.plants.Banana;
import day03.food.plants.Carrot;
import day03.food.plants.Eucalyptus;

public class FoodMaker {
    public Food createFood(Food.Products type) {
        Food food;

        switch (type) {
            case BEEF:
                food = new Beef();
                break;
            case BANANA:
                food = new Banana();
                break;
            case CARROT:
                food = new Carrot();
                break;
            case CARRION:
                food = new Carrion();
                break;
            case PLANKTON:
                food = new Plankton();
                break;
            case EUCALYPTUS:
            food = new Eucalyptus();
            break;
            default:
                throw new RuntimeException("There's no such type of food in the zoo: " + type);
        }
        return food;
    }
}

package day03.animals.birds;

import day03.Feedable;
import day03.food.Food;

/**
 * The duck is omnivorous
 */
public class Duck extends Bird implements Feedable {

    public Duck(String name) {
        super(name);
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.getRandomProduct();
    }

}

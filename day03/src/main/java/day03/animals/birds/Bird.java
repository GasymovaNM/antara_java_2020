package day03.animals.birds;

import day03.animals.Animal;
import day03.food.Food;

public abstract class Bird extends Animal {

    public Bird(String name) {
        super(name);
    }

    @Override
    protected boolean checkFood(Food food) {
        return true;
    }



}

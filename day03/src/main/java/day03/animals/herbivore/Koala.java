package day03.animals.herbivore;

import day03.Feedable;
import day03.food.Food;

public class Koala extends Herbivore implements Feedable {

    public Koala(String name) {
        super(name);
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.EUCALYPTUS;
    }
}

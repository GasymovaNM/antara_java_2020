package day03.animals.herbivore;

import day03.animals.Mammal;
import day03.food.Food;
import day03.food.plants.PlantFood;

public abstract class Herbivore extends Mammal {

    public Herbivore(String name) {
        super(name);
    }

    @Override
    protected boolean checkFood(Food food) {
        if (food instanceof PlantFood) {
            return true;
        } else {
            System.out.println("UGH! I can eat only plant food");
            return false;
        }
    }

}

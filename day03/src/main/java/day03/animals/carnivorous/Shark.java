package day03.animals.carnivorous;

import day03.Feedable;
import day03.food.Food;

public class Shark extends Carnivorous implements Feedable {

    public Shark(String name) {
        super(name);
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.PLANKTON;
    }
}

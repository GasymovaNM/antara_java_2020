package day03.animals.carnivorous;

import day03.Feedable;
import day03.food.Food;

public class Lion extends Carnivorous  implements Feedable {

    public Lion(String name) {
        super(name);
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.BEEF;
    }
}

package day03.animals;

import day03.food.Food;

public abstract class Animal {

    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public enum Animals {
        LION,
        CROCODILE,
        SHARK,
        ELEPHANT,
        RABBIT,
        KOALA,
        DUCK
    }

    public void sayName() {
        System.out.println("Hi! I am a " + getClass().getSimpleName() + ", my name is " + getName());
    }

    public void eat(Food food) {
        if (checkFood(food)) {
            System.out.println(getClass().getSimpleName() + " " + name + " is eating " + food.getClass().getSimpleName());
        }
    }

    protected abstract boolean checkFood(Food food);
}

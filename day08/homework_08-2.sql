--1--
SELECT model, speed, hd
FROM PC
WHERE price < 500;

--2--
Select distinct maker 
from product 
where type='printer';

--3--
Select model,ram,screen 
from laptop 
where price>1000;

--4--
Select * 
from printer 
where color = 'y';

--5--
Select model,speed,hd 
from pc 
where cd in ('12x','24x') 
and price <600;

--6--
SELECT distinct product.maker, laptop.speed 
FROM product 
JOIN laptop on product.model = laptop.model 
and laptop.hd >= 10;

--7--
Select p.model, price from product p
join laptop l on p.model=l.model
where maker = 'B'
union
Select p.model, price from product p
join pc on p.model=pc.model
where maker = 'B'
union
Select p.model, price from product p
join printer pr on p.model=pr.model
where maker = 'B';

--8--
SELECT DISTINCT maker
FROM product
WHERE type = 'pc'
EXCEPT
SELECT DISTINCT product.maker
FROM product
Where type = 'laptop';

--9-
SELECT DISTINCT maker FROM Product 
JOIN PC
ON product.model=pc.model
where pc.speed >=450


--10--
select model,price 
from printer
where price = (select max(price) from printer)

--11--
Select avg(speed)
 from pc;

 --12--
 select avg (speed) 
 from laptop 
 where price >1000;

 --13--
 Select avg(speed) 
 from pc 
 where model IN(SELECT model
FROM Product WHERE maker = 'A');

 --14--
Select ships.class, ships.name, cl.country 
from ships
join classes cl
on cl.class = ships.class
where numGuns in (select numGuns from Classes where numGuns >=10);

--15--
Select hd
from pc
group by hd
having count(model) >= 2

--16--
Select distinct a.model AS model, b.model AS model, A.speed As speed, A.ram As ram 
from pc as a, pc b
where a.speed=b.speed
and a.ram = b.ram
AND A.model > B.model

--17--
Select distinct p.type, l.model, l.speed
from laptop l join product p
on l.model = p.model
where l.speed < all(select speed from pc);

--18--
Select distinct product.maker, printer.price
from product join printer
on product.model = printer.model
where printer.price = (select min (price) from printer where color = 'y')
and printer.color = 'y';

--19--
Select product.maker, avg(screen) as screens
from laptop 
left join product on product.model=laptop.model
group by product.maker

--20--
Select maker,count(model)
from product 
where type like 'pc'
group by maker
having count(model) >=3

--21--
Select maker, max(price)
from product join pc
on product.model = pc.model
group by product.maker;


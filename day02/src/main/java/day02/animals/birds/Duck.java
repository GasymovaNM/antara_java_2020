package day02.animals.birds;

import day02.food.Food;

public class Duck extends Bird {

    public Duck(String name) {
        super(name);
    }
}

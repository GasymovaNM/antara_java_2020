package day02.animals.birds;

import day02.animals.Animal;
import day02.food.Food;

public abstract class Bird extends Animal {

    public Bird(String name) {
        super(name);
    }

    @Override
    protected boolean checkFood(Food food) {
        return true;
    }



}

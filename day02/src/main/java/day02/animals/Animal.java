package day02.animals;

import day02.food.Food;

public abstract class Animal {

    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public enum Animals {
        LION,
        CROCODILE,
        SHARK,
        ELEPHANT,
        RABBIT,
        KOALA,
        DUCK
    }

    public void eat(Food food) {
        if (checkFood(food)) {
            System.out.println(this.getClass().getSimpleName() + " " + name + " is eating");
        }
    }

    public void sayName() {
        System.out.println("Hi! I am a " + getClass().getSimpleName() + ", my name is " + getName());
    }

    protected abstract boolean checkFood(Food food);
}

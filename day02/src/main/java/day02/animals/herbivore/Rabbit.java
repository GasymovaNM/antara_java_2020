package day02.animals.herbivore;

public class Rabbit extends Herbivore {

    public Rabbit(String name) {
        super(name);
    }
}

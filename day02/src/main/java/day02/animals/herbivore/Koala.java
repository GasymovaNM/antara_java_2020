package day02.animals.herbivore;

public class Koala extends Herbivore {

    public Koala(String name) {
        super(name);
    }
}

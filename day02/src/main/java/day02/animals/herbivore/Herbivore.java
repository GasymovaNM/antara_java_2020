package day02.animals.herbivore;

import day02.animals.Mammal;
import day02.food.Food;
import day02.food.plants.PlantFood;

public abstract class Herbivore extends Mammal {

    public Herbivore(String name) {
        super(name);
    }

    @Override
    protected boolean checkFood(Food food) {
        if (food instanceof PlantFood) {
            return true;
        } else {
            System.out.println("UGH! I can eat only plant food");
            return false;
        }
    }

}

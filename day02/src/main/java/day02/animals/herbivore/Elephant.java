package day02.animals.herbivore;

public class Elephant extends Herbivore {

    public Elephant(String name) {
        super(name);
    }
}

package day02.animals.carnivorous;

import day02.animals.Mammal;
import day02.food.Food;
import day02.food.meat.MeatFood;

public abstract class Carnivorous extends Mammal {

    public Carnivorous(String name) {
        super(name);
    }

    @Override
    protected boolean checkFood(Food food) {
        if (food instanceof MeatFood) {
            return true;
        } else {
            System.out.println("UGH! I can eat only meat food");
            return false;
        }
    }
}

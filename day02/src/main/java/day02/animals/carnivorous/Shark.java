package day02.animals.carnivorous;

public class Shark extends Carnivorous {

    public Shark(String name) {
        super(name);
    }
}

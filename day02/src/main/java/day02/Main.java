package day02;

import day02.animals.Animal;
import day02.animals.carnivorous.Carnivorous;
import day02.animals.herbivore.Herbivore;
import day02.aviary.AviaryForCarnivores;
import day02.aviary.AviaryForHerbivore;
import day02.food.Food;

public class Main {
    public static void main(String[] args) {
        AnimalMaker animalMaker = new AnimalMaker();
        FoodMaker foodMaker = new FoodMaker();
        Food banana = foodMaker.createFood(Food.Products.BANANA);
        Food beef = foodMaker.createFood(Food.Products.BEEF);
        Food plankton = foodMaker.createFood(Food.Products.PLANKTON);
        Food eucalyptus = foodMaker.createFood(Food.Products.EUCALYPTUS);
        Food carrion = foodMaker.createFood(Food.Products.CARRION);
        Food carrot = foodMaker.createFood(Food.Products.CARROT);

        //--------------------------------- carnivorous --------------------------------
        Animal lion1 = animalMaker.createAnimal(Animal.Animals.LION, "Mufasa");
        Animal lion2 = animalMaker.createAnimal(Animal.Animals.LION, "Simba");
        Animal lion3 = animalMaker.createAnimal(Animal.Animals.LION, "Scar");
        Animal lion4 = animalMaker.createAnimal(Animal.Animals.LION, "Leo");
        Animal lion5 = animalMaker.createAnimal(Animal.Animals.LION, "Nala");
        Animal crocodile1 = animalMaker.createAnimal(Animal.Animals.CROCODILE, "Dundee");
        Animal crocodile2 = animalMaker.createAnimal(Animal.Animals.CROCODILE, "Dundee2");
        Animal shark = animalMaker.createAnimal(Animal.Animals.SHARK, "Judy");
        lion1.eat(banana);
        lion2.eat(beef);
        lion3.eat(carrion);

        AviaryForCarnivores aviaryForCarnivores1 = new AviaryForCarnivores(4);
        AviaryForCarnivores aviaryForCarnivores2 = new AviaryForCarnivores(3);

        aviaryForCarnivores1.addAnimal((Carnivorous) lion1);
        aviaryForCarnivores1.addAnimal((Carnivorous) lion2);
        aviaryForCarnivores1.addAnimal((Carnivorous) lion3);
        aviaryForCarnivores1.addAnimal((Carnivorous) lion4);
        aviaryForCarnivores1.addAnimal((Carnivorous) lion5);
        aviaryForCarnivores1.sayAnimalNames();

        aviaryForCarnivores2.addAnimal((Carnivorous) crocodile1);
        aviaryForCarnivores2.addAnimal((Carnivorous) crocodile2);
        aviaryForCarnivores2.addAnimal((Carnivorous) shark);
        aviaryForCarnivores2.sayAnimalNames();


        //--------------------------------- herbivore --------------------------------
        Animal duck = animalMaker.createAnimal(Animal.Animals.DUCK, "Donalt");
        duck.eat(plankton);

        Animal koala = animalMaker.createAnimal(Animal.Animals.KOALA, "Martin");
        Animal rabbit = animalMaker.createAnimal(Animal.Animals.RABBIT, "Rodger");
        Animal elephant1 = animalMaker.createAnimal(Animal.Animals.ELEPHANT, "Dumbo");
        Animal elephant2 = animalMaker.createAnimal(Animal.Animals.ELEPHANT, "Dumbo2");
        koala.eat(beef);
        koala.eat(eucalyptus);
        rabbit.eat(carrot);
        elephant1.eat(banana);

        AviaryForHerbivore aviaryForHerbivore1 = new AviaryForHerbivore(2);
        AviaryForHerbivore aviaryForHerbivore2 = new AviaryForHerbivore(2);

        aviaryForHerbivore1.addAnimal((Herbivore) koala);
        aviaryForHerbivore1.addAnimal((Herbivore) rabbit);
        aviaryForHerbivore1.sayAnimalNames();

        aviaryForHerbivore2.addAnimal((Herbivore) elephant1);
        aviaryForHerbivore2.addAnimal((Herbivore) elephant2);
        aviaryForHerbivore2.sayAnimalNames();
    }
}

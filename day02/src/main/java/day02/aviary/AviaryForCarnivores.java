package day02.aviary;

import day02.animals.carnivorous.Carnivorous;

public class AviaryForCarnivores extends Aviary {

    public AviaryForCarnivores(int capacity) {
        super(capacity);
    }

    public void addAnimal(Carnivorous c) {
        if (!super.addAnimal(c)) {
            System.out.println("Aviary for carnivores is full, there is no space for " + c.getName());
        }
    }


}

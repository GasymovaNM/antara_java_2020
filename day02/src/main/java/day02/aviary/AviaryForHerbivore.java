package day02.aviary;

import day02.animals.herbivore.Herbivore;

public class AviaryForHerbivore extends Aviary {

    public AviaryForHerbivore(int capacity) {
        super(capacity);
    }

    public void addAnimal(Herbivore h) {
        if (!super.addAnimal(h)) {
            System.out.println("Aviary for herbivores is full, there is no space for " + h.getName());
        }
    }

}

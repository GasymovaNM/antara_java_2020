package day02.aviary;

import java.util.ArrayList;
import java.util.List;
import day02.animals.Animal;

public abstract class Aviary {

    private int size;

    private List<Animal> animals;

    public Aviary(int capacity) {
        this.animals = new ArrayList<Animal>(capacity);
        this.size = capacity;
    }

    public void sayAnimalNames() {
        for (Animal a : animals) {
            a.sayName();
        }
    }

    protected boolean addAnimal(Animal a) {
        if (animals.size() < size) {
            animals.add(a);
            return true;
        } else {
            return false;
        }
    }
}

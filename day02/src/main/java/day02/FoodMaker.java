package day02;

import day02.food.Food;
import day02.food.meat.Beef;
import day02.food.meat.Carrion;
import day02.food.meat.Plankton;
import day02.food.plants.Banana;
import day02.food.plants.Carrot;
import day02.food.plants.Eucalyptus;

public class FoodMaker {
    public Food createFood(Food.Products type) {
        Food food;

        switch (type) {
            case BEEF:
                food = new Beef();
                break;
            case BANANA:
                food = new Banana();
                break;
            case CARROT:
                food = new Carrot();
                break;
            case CARRION:
                food = new Carrion();
                break;
            case PLANKTON:
                food = new Plankton();
                break;
            case EUCALYPTUS:
            food = new Eucalyptus();
            break;
            default:
                throw new RuntimeException("There's no such type of food in the zoo: " + type);
        }
        return food;
    }
}

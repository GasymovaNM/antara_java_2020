public class Main {
    private static final int[] INTS = {5, 7, 8, 9, 0, 1, 4};
    private static final char[] CHARS = {'a', 'n', 't', 'a', 'r', 'a'};
    private static final long[] LONGS = {4L, 2L, 10L, 1L, 5L};
    private static final String[] STRINGS = {"winter", "spring", "summer", "autumn"};

    /**
     * Проходимся по массиву и выводим сумму всех элементов
     */
    private static void printSum(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        System.out.println(sum);
    }

    /**
     * Конкатенация символов
     */
    private static void printChars(char[] arr) {
        int i = 0;
        StringBuilder result = new StringBuilder();
        while (i < arr.length) {
            result.append(arr[i]);
            i++;
        }
        System.out.println(result);
    }

    /**
     * Проходимся по массиву с конца и возводим каждый элемент в квадрат
     */
    private static void printSquaresReverse(long[] arr) {
        int i = arr.length - 1;
        do {
            if (i < 0) {
                System.out.println("Массив пустой");
                break;
            }
            long squareNumber = arr[i] * arr[i];
            System.out.printf("%d, ", squareNumber);
            i--;
        }
        while (i >= 0);
        System.out.println();
    }

    /**
     * Перевод всех элементов (строк) в верхний регистр
     */
    private static void printStringsUpperCase(String[] arr) {
        for (String season : arr) {
            String s = season.toUpperCase();
            System.out.printf("%s, ", s);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        printSum(INTS);
        printChars(CHARS);
        printSquaresReverse(LONGS);
        printStringsUpperCase(STRINGS);
    }
}

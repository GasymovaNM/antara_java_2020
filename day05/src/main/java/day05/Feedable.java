package day05;

import day05.food.Food;

public interface Feedable {
    void eat(Food food);

    Food.Products sayWhatToEat();

}

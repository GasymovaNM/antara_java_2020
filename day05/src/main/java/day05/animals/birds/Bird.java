package day05.animals.birds;

import day05.animals.Animal;
import day05.food.Food;

public abstract class Bird extends Animal {

    public Bird(String name) {
        super(name);
    }

    @Override
    protected void checkFood(Food food) {
        throw new UnsupportedOperationException();
    }



}

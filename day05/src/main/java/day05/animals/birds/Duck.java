package day05.animals.birds;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import day05.Feedable;
import day05.animals.AnimalSize;
import day05.food.Food;

/**
 * The duck is omnivorous
 */
public class Duck extends Bird implements Feedable {
    private static Logger logger = LoggerFactory.getLogger(Duck.class);

    public Duck(String name) {
        super(name);
        setSize(AnimalSize.SMALL);
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.getRandomProduct();
    }

    public void eat(Food food) {
        logger.info (getClass().getSimpleName() + " " + getName() + " is eating " + food.getClass().getSimpleName());
    }
}

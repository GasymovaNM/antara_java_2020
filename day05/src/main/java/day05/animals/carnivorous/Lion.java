package day05.animals.carnivorous;

import day05.Feedable;
import day05.animals.AnimalSize;
import day05.food.Food;

public class Lion extends Carnivorous  implements Feedable {

    public Lion(String name) {
        super(name);
        setSize(AnimalSize.MIDDLE);
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.BEEF;
    }
}

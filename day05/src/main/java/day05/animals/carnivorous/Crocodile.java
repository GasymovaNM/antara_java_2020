package day05.animals.carnivorous;

import day05.Feedable;
import day05.animals.AnimalSize;
import day05.food.Food;

public class Crocodile extends Carnivorous implements Feedable {

    public Crocodile(String name) {
        super(name);
        setSize(AnimalSize.BIG);
    }


    public Food.Products sayWhatToEat() {
        return Food.Products.CARRION;
    }
}

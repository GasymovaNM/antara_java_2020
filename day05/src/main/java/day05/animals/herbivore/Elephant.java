package day05.animals.herbivore;

import day05.Feedable;
import day05.animals.AnimalSize;
import day05.food.Food;

public class Elephant extends Herbivore implements Feedable {

    public Elephant(String name) {
        super(name);
        setSize(AnimalSize.LARGE);
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.BANANA;
    }
}

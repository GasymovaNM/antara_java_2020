package day05.animals.herbivore;

import day05.Feedable;
import day05.animals.AnimalSize;
import day05.food.Food;

public class Rabbit extends Herbivore implements Feedable {

    public Rabbit(String name) {
        super(name);
        setSize(AnimalSize.SMALL);
    }

    public Food.Products sayWhatToEat() {
        return Food.Products.CARROT;
    }
}

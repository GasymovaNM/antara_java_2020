package day05.animals.herbivore;

import day05.food.WrongFoodException;
import day05.animals.Mammal;
import day05.food.Food;
import day05.food.plants.PlantFood;

public abstract class Herbivore extends Mammal {

    public Herbivore(String name) {
        super(name);
    }

    @Override
    protected void checkFood(Food food) throws WrongFoodException {
        boolean isSuitableFood = food instanceof PlantFood;
        if (!isSuitableFood) {
            throw new WrongFoodException();
        }
    }

}

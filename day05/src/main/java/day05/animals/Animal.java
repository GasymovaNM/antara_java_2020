package day05.animals;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import day05.food.WrongFoodException;
import day05.food.Food;

public abstract class Animal {
    private static Logger logger = LoggerFactory.getLogger(Animal.class);

    private String name;
    private AnimalSize size;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    protected void setSize(AnimalSize size) {
        this.size = size;
    }

    public AnimalSize getSize() {
        return size;
    }

    public enum Animals {
        LION,
        CROCODILE,
        SHARK,
        ELEPHANT,
        RABBIT,
        KOALA,
        DUCK
    }

    public void sayName() {
        logger.info("Hi! I am a " + getClass().getSimpleName() + ", my name is " + getName());
    }

    public void eat(Food food) {
        try {
            checkFood(food);
            logger.info(getClass().getSimpleName() + " " + name + " is eating " + food.getClass().getSimpleName());
        } catch (WrongFoodException e) {
            logger.error("UGH! I can't eat this food: " + food.getClass().getSimpleName());
        }
    }

    protected abstract void checkFood(Food food) throws WrongFoodException;
}
